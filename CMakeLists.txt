cmake_minimum_required(VERSION 3.9)
project(BLACKJACK)

set(CMAKE_CXX_STANDARD 11)

add_executable(BLACKJACK main.cpp)